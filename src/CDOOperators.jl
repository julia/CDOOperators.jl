
"""
Module that contains CDO Operators based on the cdi.jl package.
"""
module CDOOperators

import Libdl

# create libcdo symbole
@static if Sys.iswindows()
    @error "Windows is currently not supported"
    const libcdo = ""
elseif Sys.isapple()
    const libcdo = Libdl.find_library(["libcdo", "cdo"])
elseif Sys.islinux()
    const libcdo = Libdl.find_library(["libcdo", "cdo"])
else
    @warn "Your platform couldn't detected. Use the fallback"
    const libcdo = Libdl.find_library(["libcdo", "cdo"])
end

@assert libcdo != "" "Can't load libcdo at DL_LOAD_PATH"

# init the module by first load
function __init__()
    @info "init cdo"
    ccall((:cdo_init, libcdo), Cvoid, (),)
end

# include operators
include("spectral.jl")

end # module


import cdi.cdic: libcdi
import cdi.cdiDatasets: AbstractGrid

function cdo_nlat2ntr(nlat::Integer)
    return ccall((:cdo_nlat2ntr, libcdo), Cint, (Cint,), nlat)
end
export cdo_nlat2ntr

function cdo_spectrans_new(nlon::Integer, nlat::Integer, ntr::Integer)
    return ccall((:cdo_spectrans_new, libcdo), Ptr{Cvoid}, (Csize_t, Csize_t, Csize_t), nlon, nlat, ntr)
end
export cdo_spectrans_new

function cdo_spectrans_delete(spectrans::Ptr{Cvoid})
    return ccall((:cdo_spectrans_delete, libcdo), Cvoid, (Ptr{Cvoid},), spectrans)
end
export cdo_spectrans_delete

function cdo_grid_to_spec(spectrans::Ptr{Cvoid}, 
    gridIn::AbstractGrid, dataIn::AbstractArray{Float64, 2}, 
    gridOut::AbstractGrid, dataOut::AbstractArray{Float64, 1})
    @assert Base.viewindexing(dataIn.indices) == IndexLinear()
    @assert Base.viewindexing(dataOut.indices) == IndexLinear()
    ccall((:cdo_grid_to_spec, libcdo), Cvoid, (Ptr{Cvoid}, Cint, Ptr{Float64}, Cint, Ptr{Float64}), 
        spectrans, gridIn.gridID, view(dataIn, :), gridOut.gridID, dataOut)
end
export cdo_grid_to_spec
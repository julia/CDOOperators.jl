using CDOOperators

# import the c-libcdi Enums like TIME.VARYING and FileTypes.FILETYPE_NC
using cdi

# import the high-level libcdi wraper
using cdi.cdiDatasets

import Base.OneTo

# help function to test a variable name
hasVar(ds, name) = haskey(ds, name) || (@error "$name is not define" && exit(1))

function gridpoint2spectral(context, inputVar, inputData, outputVar)
    gridIn = getGrid(inputVar)
    gridOut = getGrid(outputVar)

    #outputData = Array{Float64, 2}(undef, size(outputVar)...)
    outputData = fill(0.0, size(outputVar)...)

    for k in OneTo(size(getZAxis(inputVar))...)
        cdo_grid_to_spec(spectrans, gridIn, view(inputData, :, :, k), gridOut, view(outputData, :, k))
    end
    
    return outputData
end

const inputPath = joinpath(@__DIR__, "example_gp.grb")
const outputPath = joinpath(@__DIR__, "result_sp.grb")

# laod *.grb file to read the data
input = cdiDataset(inputPath)
hasVar(input, "aps")

if typeof(input["aps"].grid) != Gaussian
    @error "Input data on Gaussian grid needed!"
    exit(1)
end

# create output file
tAxis = TAxisAbsolut()
ntr = cdo_nlat2ntr(size(input["aps"])[2])
grid = Spectral(ntr)
varTemplate = Variable("aps", grid, input["aps"].zAxis, TIME.VARYING)
output = cdiDataset(outputPath, FileTypes.FILETYPE_GRB, tAxis, [varTemplate])

# create context
spectrans = cdo_spectrans_new(size(input["aps"])[1], size(input["aps"])[2], ntr)
@info typeof(spectrans)

# loop over the time steps 
while getNextTimestep(input)
    # log
    @info "Step " * string(getTimestep(input))

    # read variable
    var = loadData(input["aps"])
    println("Min: ", min(reshape(var, :)...), "  Max: ", max(reshape(var, :)...))

    out = gridpoint2spectral(spectrans, input["aps"], var, output["aps"])
    println("Min: ", min(reshape(out, :)...), "  Max: ", max(reshape(out, :)...))

    storeData!(output["aps"], out)
end

cdo_spectrans_delete(spectrans)
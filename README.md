# CDOOperators.jl

Julia wrapper around cdo 

## install
Firste follow the install instructions [here](https://gitlab.dkrz.de/m300859/cdi.jl/-/blob/master/README.md).
```
git clone --recursive git@gitlab.dkrz.de:mpim-sw/cdo.git
cd cdo
git checkout dev-heidmann
git submodule update
cd libcdi/
git checkout develop
cd ..
autoreconf -i
./configure --enable-cdo-lib --with-fftw3 --prefix=$HOME/local/ LDFLAGS="-L/home/vneff/local/lib -lcdi"
make
```

## run example
Change into the project root folder.
```
julia --project=@. examples/cdo_gridpoint2spectral.jl
```